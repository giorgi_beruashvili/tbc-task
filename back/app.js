const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose');


// Route imports
const accountRoute = require('./api/routes/account.route');
const clientRoute = require('./api/routes/client.route');

mongoose.connect('mongodb://localhost:27017/tbc_task', {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
}, (error) => {
  if(!error) console.log('Connected to DB!');
  else console.log(error);
});

app.use(express.static('upload'));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

// Routes to handle requests
app.use('/api/account', accountRoute);
app.use('/api/client', clientRoute);


app.use((req, res, next) => {
    res.status(404).json({
        error: 'Not Found'
    });
});

module.exports = app;