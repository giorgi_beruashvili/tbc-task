const dotenv = require('dotenv');
const app = require('./app');

dotenv.config();

const port = process.env.PORT || 3000;

const server = app.listen(port, () => {
    console.log(`Server has successfully started on port: ${port}`);
});