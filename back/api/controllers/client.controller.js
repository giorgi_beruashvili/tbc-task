const Client = require('../models/Client.model');
const fs = require('fs');
const path = require('path');

module.exports.createClient = async (req, res, next) => {
    try {
        req.body.formalLocation = JSON.parse(req.body.formalLocation);
        req.body.physicalLocation = JSON.parse(req.body.physicalLocation);
        if(req.file) {
            req.body['photoId'] = req.file.filename;
        }
        const client = await Client.create(req.body);
        res.status(200).json(client);
    } catch (error) {
        if(error.code === 11000) {
            const imgPath = path.join(__dirname + '..', '..', '..', 'upload', 'images', req.file.filename);
            fs.unlinkSync(imgPath);
        }
        res.status(500).json({
            error
        });
    }
}

module.exports.editClient = async (req, res, next) => {
    try {
        const prevClient = await Client.findById(req.params.id);
        if(prevClient.photoId) {
            const imgPath = path.join(__dirname + '..', '..', '..', 'upload', 'images', prevClient.photoId);
            fs.unlinkSync(imgPath);
        }

        req.body.formalLocation = JSON.parse(req.body.formalLocation);
        req.body.physicalLocation = JSON.parse(req.body.physicalLocation);
        if(req.file) {
            req.body['photoId'] = req.file.filename;
        }
        const client = await Client.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.status(200).json(client);
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error
        });
    }
}

module.exports.deleteClient = async (req, res, next) => {
    try {
        const prevClient = await Client.findById(req.params.id);
        if(prevClient.photoId) {
            const imgPath = path.join(__dirname + '..', '..', '..', 'upload', 'images', prevClient.photoId);
            fs.unlinkSync(imgPath);
        }
        const client = await Client.findByIdAndDelete(req.params.id);
        res.status(200).json(client);
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error
        });
    }
}

module.exports.getClients = async (req, res, next) => {
    try {
        const clients = await Client.find(req.body).skip(Number(req.query.skip)).limit(Number(req.query.limit));
        res.status(200).json(clients);
    } catch (error) {
        res.status(500).json({
            error
        });
    }
}

module.exports.getClientsNum = async (req, res, next) => {
    try {
        const clientsNum = await Client.countDocuments(req.body);
        res.status(200).json({
            count: clientsNum
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            error
        });
    }
}