const Client = require('../models/Client.model');
const Account = require('../models/Account.model');

module.exports.createAccount = async (req, res, next) => {
    try {
        const account = await Account.create({
            ...req.body,
            clientId: req.params.id
        });
        res.status(200).json(account);
    } catch (error) {
        res.status(500).json({
            error
        });
    }
}

module.exports.deleteAccount = async (req, res, next) => {
    try {
        const account = await Account.findByIdAndDelete(req.params.id);
        res.status(200).json(account);
    } catch (error) {
        res.status(500).json({
            error
        });
    }
}

module.exports.getAccounts = async (req, res, next) => {
    try {
        const accounts = await Account.find({ clientId: req.params.id });
        res.status(200).json(accounts);
    } catch (error) {
        res.status(500).json({
            error
        });
    }
}

module.exports.updateAccountStatus = async (req, res, next) => {
    try {
        const account = await Account.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.status(200).json(account);
    } catch (error) {
        res.status(500).json({
            error
        });
    }
}