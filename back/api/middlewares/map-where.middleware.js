const mongoose = require('mongoose');

const clientMap = {
    _id: (data) => mongoose.Types.ObjectId(data),
    clientId: (data) => data,
    name: (data) => { return { "$regex": data, "$options": "i"}},
    surname: (data) => { return { "$regex": data, "$options": "i"}},
    gender: (data) => data,
    privateId: (data) => { return { "$regex": data, "$options": "i"}},
    phone: (data) => { return { "$regex": data, "$options": "i"}}
}

module.exports.client = async (req, res, next) => {
    for(const key in req.body) {
        req.body[key] = clientMap[key](req.body[key]);
    }
    next();
}