const mongoose = require('mongoose');

const accountSchema = mongoose.Schema({
    accountId: {
        type: Number,
        unique: true
    },
    clientId: {
        type: Number,
        required: true
    },
    accountType: {
        type: String,
        required: true,
        enum: ['InProgress', 'Saving', 'Collecting']
    },
    currency: {
        type: String,
        required: true,
        enum: ['GEL', 'USD', 'EUR', 'RUB']
    },
    accountStatus: {
        type: String,
        required: true,
        enum: ['Active', 'Closed']
    }
});

module.exports = mongoose.model('Account', accountSchema);