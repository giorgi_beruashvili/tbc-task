const mongoose = require('mongoose');

const clientSchema = mongoose.Schema({
    clientId: {
        type: Number,
        unique: true
    },
    name: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50,
        match: /^(([a-zA-Z]+)|([\u10A0-\u10FF]+))$/
    },
    surname: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 50,
        match: /^(([a-zA-Z]+)|([\u10A0-\u10FF]+))$/
    },
    gender: {
        type: String,
        required: true,
        enum: ['Female', 'Male']
    },
    privateId: {
        type: String,
        required: true,
        validate: {
            validator: (v) => {
                return v.length === 11;
            },
            message: 'Must be of length 11'
        }
    },
    phone: {
        type: String,
        required: true,
        match: /^5/,
        validate: {
            validator: (v) => {
                return v.length === 9;
            },
            message: 'Must be of length 9'
        }
    },
    formalLocation: {
        country: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        address: {
            type: String,
            required: true
        }
    },
    physicalLocation: {
        country: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        address: {
            type: String,
            required: true
        }
    },
    photoId: {
        type: String
    }
});

module.exports = mongoose.model('Client', clientSchema);