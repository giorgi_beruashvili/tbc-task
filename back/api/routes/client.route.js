const router = require('express').Router();
const multer = require('multer');
const clientController = require('../controllers/client.controller');
const mapWhereMiddleware = require('../middlewares/map-where.middleware');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './upload/images');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname.replace(/ /g, ''));
    }
});
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    }
});

router.post('/', upload.single('photo'), clientController.createClient);
router.put('/:id', upload.single('photo'), clientController.editClient);
router.delete('/:id', clientController.deleteClient);
router.patch('/', mapWhereMiddleware.client, clientController.getClients);
router.patch('/count', mapWhereMiddleware.client, clientController.getClientsNum);

module.exports = router;