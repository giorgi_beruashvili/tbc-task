const router = require('express').Router();
const accountController = require('../controllers/account.controller');

router.post('/:id', accountController.createAccount);
router.delete('/:id', accountController.deleteAccount);
router.get('/:id', accountController.getAccounts);
router.put('/:id', accountController.updateAccountStatus);

module.exports = router;